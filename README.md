# SIKA Contact Management

## Running the project

The project is implemented using SpringBoot with maven as the build mechanism.
To build and run it, the following commands can be executed in its root directory:

* Building: `mvn clean install`
* Running: `mvn spring-boot:run`

The project is configured to run on port `16000` which can be changed
in the `application.properties` file.

--------

## Development
* API Documentation [Swagger](http://localhost:16000/contact/swagger-ui/index.html#) : http://localhost:16000/contact/swagger-ui/index.html#

![Screen_Shot_2023-01-27_at_5.19.56_AM](/uploads/6b621c3d2c10a802d63e051761ed4edf/Screen_Shot_2023-01-27_at_5.19.56_AM.png)


* Unit Test 90% [Test Coverage](http://localhost:63342/sikatest-master/contact/target/site/jacoco/index.html) : http://localhost:63342/sikatest-master/contact/target/site/jacoco/index.html

![Screen_Shot_2023-01-27_at_5.04.29_AM](/uploads/7a10cefa6e6aedcb44df8ead0a6938a3/Screen_Shot_2023-01-27_at_5.04.29_AM.png)


--------
