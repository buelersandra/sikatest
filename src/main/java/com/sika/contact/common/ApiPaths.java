package com.sika.contact.common;

public class ApiPaths {

    private ApiPaths(){

    }
    /**
     * The constant VERSION.
     */
    public static final String VERSION = "/v1";

    /**
     * RESPONSE MESSAGES.
     */
    public static final String SUCCESS_MSG = "SUCCESS";
    public static final String FAILED_MSG = "FAILED";

    public static final String NOT_FOUND_MSG = "Contact not found";
}
